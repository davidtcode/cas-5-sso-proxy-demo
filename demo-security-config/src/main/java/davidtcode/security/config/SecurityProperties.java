package davidtcode.security.config;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

/**
 * @author David.Tegart
 */
@Configuration
@ConfigurationProperties(prefix = "demo.security", ignoreUnknownFields = false)
@Validated
public class SecurityProperties {

	@NotNull
	private String serviceUrlForCas;

	@NotNull
	private String proxyCallbackUrl;

	@NotNull
	private String proxyReceptorPath;

	@NotNull
	private String logoutPath;

	@NotNull
	private String logoutSuccessUrl;

	@NotNull
	private String casUrlPrefix;

	@NotNull
	private String casLoginUrl;

	@NotNull
	private String casLogoutUrl;
	
	private CasAttributes casAttributes = new CasAttributes();

	public String getServiceUrlForCas() {
		return serviceUrlForCas;
	}

	public void setServiceUrlForCas(String serviceUrlForCas) {
		this.serviceUrlForCas = serviceUrlForCas;
	}

	public String getProxyCallbackUrl() {
		return proxyCallbackUrl;
	}

	public void setProxyCallbackUrl(String proxyCallbackUrl) {
		this.proxyCallbackUrl = proxyCallbackUrl;
	}

	public String getProxyReceptorPath() {
		return proxyReceptorPath;
	}

	public void setProxyReceptorPath(String proxyReceptorPath) {
		this.proxyReceptorPath = proxyReceptorPath;
	}

	public String getLogoutPath() {
		return logoutPath;
	}

	public void setLogoutPath(String logoutPath) {
		this.logoutPath = logoutPath;
	}

	public String getLogoutSuccessUrl() {
		return logoutSuccessUrl;
	}

	public void setLogoutSuccessUrl(String logoutSuccessUrl) {
		this.logoutSuccessUrl = logoutSuccessUrl;
	}

	public String getCasUrlPrefix() {
		return casUrlPrefix;
	}

	public void setCasUrlPrefix(String casUrlPrefix) {
		this.casUrlPrefix = casUrlPrefix;
	}

	public String getCasLoginUrl() {
		return casLoginUrl;
	}

	public void setCasLoginUrl(String casLoginUrl) {
		this.casLoginUrl = casLoginUrl;
	}

	public String getCasLogoutUrl() {
		return casLogoutUrl;
	}

	public void setCasLogoutUrl(String casLogoutUrl) {
		this.casLogoutUrl = casLogoutUrl;
	}

	public CasAttributes getCasAttributes() {
		return casAttributes;
	}

	public void setCasAttributes(CasAttributes casAttributes) {
		this.casAttributes = casAttributes;
	}

	public static class CasAttributes {
		
		private List<String> allowedAttributes = new ArrayList<>();

		public List<String> getAllowedAttributes() {
			return allowedAttributes;
		}

		public void setAllowedAttributes(List<String> allowedAttributes) {
			this.allowedAttributes = allowedAttributes;
		}

		@Override
		public String toString() {
			StringBuilder builder = new StringBuilder();
			builder.append("CasAttributes [allowAttributes=");
			builder.append(allowedAttributes);
			builder.append("]");
			return builder.toString();
		}
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SecurityProperties [serviceUrlForCas=");
		builder.append(serviceUrlForCas);
		builder.append(", proxyCallbackUrl=");
		builder.append(proxyCallbackUrl);
		builder.append(", proxyReceptorPath=");
		builder.append(proxyReceptorPath);
		builder.append(", logoutPath=");
		builder.append(logoutPath);
		builder.append(", logoutSuccessUrl=");
		builder.append(logoutSuccessUrl);
		builder.append(", casUrlPrefix=");
		builder.append(casUrlPrefix);
		builder.append(", casLoginUrl=");
		builder.append(casLoginUrl);
		builder.append(", casLogoutUrl=");
		builder.append(casLogoutUrl);
		builder.append(", casAttributes=");
		builder.append(casAttributes);
		builder.append("]");
		return builder.toString();
	}
}