## Demonstration of CAS's SSO and Proxy Authentication ##


This is a demonstration of how to configure a Spring Security application to enable Proxy Authentication (as well as SSO or Single Sign On) for CAS (Central Authentication Service). 

The following quote from the CAS documentation succinctly describes why Proxy Authentication is often required:

> One of the more common use cases of proxy authentication is the ability to obtain a ticket for a back-end [REST-based] service that is also protected by CAS. The scenario usually is:
>
> - User is faced with application A which is protected by CAS.
> - Application A on the backend needs to contact a service S to produce data.
> - Service S itself is protected by CAS itself.

> Because A contacts service S via a server-to-service method where no browser is involved, service S would not be able to recognize that an SSO session already exists. In these cases, application A needs to exercise proxying in order to obtain a proxy ticket for service S. The proxy ticket is passed to the relevant endpoint of service S so it can retrieve and validate it via CAS and finally produce a response.


Configuring the required beans and their dependencies, setting up SSL, modifying the CAS Maven Overlay isn't straightforward, so setting this out in a relatively simple, stripped-back demo, is of benefit.

The demo comprises the CAS Server, an imaginary student Portal (application A above) and two REST-based services (applications S above). The user accesses the Portal and is redirected to a login page served up by CAS. On successful authentication, the Portal dashboard loads, presenting student-related information which is fetched from those 2, secured, REST-based applications. That is Proxy Authentication in action. (SSO is also active as, post login, those services can be accessed _directly_, without the user needing to login.)

Useful links:

https://apereo.github.io/cas/5.2.x/protocol/CAS-Protocol.html

https://apereo.github.io/cas/5.2.x/installation/Configuring-Proxy-Authentication.html

https://apereo.github.io/cas/5.2.x/planning/Architecture.html

https://apereo.github.io/cas/4.1.x/integration/Attribute-Release.html

https://docs.spring.io/spring-security/site/docs/5.0.0.RELEASE/reference/htmlsingle/#cas


### The code of interest ###

The 4 projects are:

* **_demo-cas-overlay_**

     A Maven Overlay is used to configure the CAS Server. See [Maven Overlay Installation](https://apereo.github.io/cas/5.2.x/installation/Maven-Overlay-Installation.html) for details.
     
     
     The file [etc/cas/config/cas.properties](etc/cas/config/cas.properties) defines the properties used to configure CAS for our own custom needs. It is external to the WAR file in this case, but it needn't be. Versions earlier than CAS 5 defined this information in Spring XML files. But the latest versions are Spring Boot applications which allow for customization through the use of simple properties, as opposed to configured Spring beans. Understanding these properties is key to customizing CAS, see [Configuration Properties](https://apereo.github.io/cas/5.2.x/installation/Configuration-Properties.html)
     
     CAS Services are defined in Json files at [demo-cas-overlay/src/main/webapp/WEB-INF/classes/services](demo-cas-overlay/src/main/webapp/WEB-INF/classes/services). Typically these are stored in an external data-source.
     
     The default (Thymeleaf) login page is modified at [demo-cas-overlay/src/main/webapp/WEB-INF/classes/templates](demo-cas-overlay/src/main/webapp/WEB-INF/classes/templates)
     
     Note for demo purposes, CAS is configured to read user information from a HSQLDB instance. 
     

* **_demo-security-config_**

     A library which defines the Spring Security configuration required to enable CAS SSO/Proxy Authentication. This is used by the 3 CAS clients (or Services if you will): the Portal and the 2 service apps.
     
     The configuration class [CasAuthenticationConfig](demo-security-config/src/main/java/davidtcode/security/config/CasAuthenticationConfig.java) is the core of the demo. It configures the necessary Spring Security providers, entry points, filters, user details service and so on. 
     
     Of note is ```Cas30ProxyTicketValidator``` which is the ticket validator for version 3.0 of the protocol. It, amongst other things, allows for user attributes (e.g. name, student Id as in this demo) to be included in the successful response payload, instead of requiring a separate call.
     
     Note also ```StatelessTicketCache``` which caches CAS service and Proxy tickets for stateless connections (connections where no session is present.) This is an optional, but highly beneficial, addition to how tickets are handled for the portal-to-service (Proxy-based) communication, as it prevents a round-trip to the CAS Server to validate Proxy tickets, every-time this communication takes place.
     
     Finally, note that implementation of [CasAuthenticationFacade](demo-security-config/src/main/java/davidtcode/security/CasAuthenticationFacade.java) (a collection of authentication related utilities.) It implements the ```getProxyTicket``` method. All calls to the two secured services need a Proxy ticket and generating one using ```org.jasig.cas.client.authentication.AttributePrincipal.getProxyTicketFor``` involves a call to the CAS Server. Such tickets, however, can be reused and the implementation provides a simple cache of these tickets, keyed on the service Url.
     
     The class [WebSecurityConfig](demo-security-config/src/main/java/davidtcode/security/config/WebSecurityConfig.java) is responsible for hooking the previously defined entry point and filter into the web security of the given application. 
     
     
     
* **_demo-portal_**

    A Spring Boot executable Jar. Serves up a simple dashboard presenting information obtained from the two services, accessed using CAS's Proxy Authentication. Note the necessary SSL-related properties defined in ```application.properties```.
    
    It is important to note that each call to the service needs to be accompanied by a Proxy ticket. See how the Url is built up in [BaseService](demo-portal/src/main/java/davidtcode/service/BaseService.java)
    

* **_demo-service_**
    
    A Spring Boot executable Jar. Contains 2 REST endpoints accessed by the Portal. Doubles as the two services described above. Note the necessary SSL-related properties defined in application.properties.



### Requirements ###

You will need:

* Java 8 installed (Maven isn't required as a Maven Wrapper is included)
* 4 free ports - 3000, 3001, 3002, 3003

    If these are not free, open ```demo.cmd``` and change the environment variables defined at the top.

### Deploying the demo ###

To run the demo you will need to:

* Clone the repository
* Navigate to the base folder and install the self-signed into Java's trust-store by running the command:


    **keytool -importcert -file ./etc/cas/ssl/casDemo.cer -alias casDemo -keystore %JAVA_HOME%/jre/lib/security/cacerts -keypass changeit -storepass changeit**


    All communication with the CAS Server is over HTTPS - which isn't optional - and a self-signed cert will serve our purposes here. When accessing the portal through a browser you will be explicitly asked to accept the cert. However, this is not possible when the internal http client communicates with CAS - in this instance the client can only accept the cert if it resides in its trust-store. Hence the above command. 

* Build the code with the command:

    **demo.cmd package**

    There's nothing special going on here - this will just do a "mvn clean install" for the four projects. If you're not on Windows, just run "mvnw(.cmd) clean install". 

* Run the demo with the command:

    **demo.cmd run**

    As mentioned above, this will kick-off the CAS server (which may take several minutes to start), the Portal and the 2 services. 

* Open a browser and hit https://localhost:3001/

    The browser should warn you about the certificate, which you will need to accept in order to proceed. The Portal, given that you're not authenticated, will re-direct you to the login page, served up by the CAS Server. Login with the credentials: **demo@demo.com / password**.
    
    On submission of these credentials, a series of calls will take place between the Portal app and the CAS Server. See the protocol documentation for exact details.
    
    On successful login, the Portal dashboard page is displayed. It shows the CAS attributes for the given user, including the name and student Id. Note that these attributes are defined in the table ```user_attributes```. See [setup.sql](etc/cas/sql/setup.sql)
    
    It will also show fictitious status messages from the two simple services, which the Portal accesses _on behalf_ of the user. As these services require authentication, CAS's Proxy Authentication feature is used. 
    
    Note that SSO is also in effect here: once logged into the Portal it is possible to access the other two services without explicitly needing to authenticate. Post login, hit https://localhost:3002/api/email/status/ to see this in action, assuming you haven't changed the default port number. 

### Further Considerations ###

The passwords aren't encoded. See the CAS documentation for how to configure this.

It isn't clear if Proxy tickets can be cached indefinitely. If they can't, a more advanced cache would be needed.

The portal-to-service communication here is _on-demand_ - the communication is triggered by an already-authenicated user. For the use-case where a _background_ process accesses the secured services, an alternative strategy would be required. 


### Creation of the SSL self-signed cert ###

The following keytool commands to create the self-signed cert are:

```
// Create the cert in a keystore, export the cert and import into a trust-store
keytool -genkeypair -alias casDemo -keyalg RSA -keypass changeit -storepass changeit -keystore keystore.jks -validity 11111 -dname CN=localhost -ext SAN=dns:localhost,ip:127.0.0.1
keytool -exportcert -alias casDemo -storepass changeit -keystore keystore.jks -file casDemo.cer
keytool -import -v -trustcacerts -alias casDemo -file casDemo.cer -keystore truststore.jks -keypass changeit -storepass changeit
```

And as described before, this needs to be added to Java's own truststore:

```
keytool -importcert -file ./etc/cas/ssl/casDemo.cer -alias casDemo -keystore %JAVA_HOME%/jre/lib/security/cacerts -keypass changeit -storepass changeit
```