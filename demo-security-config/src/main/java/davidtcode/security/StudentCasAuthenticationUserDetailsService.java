package davidtcode.security;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.cas.authentication.CasAssertionAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toList;

import static davidtcode.security.StudentUserDetails.ATTRIBUTE_NAME;
import static davidtcode.security.StudentUserDetails.ATTRIBUTE_STUDENT_ID;
import static davidtcode.security.StudentUserDetails.ROLE_STUDENT;

/**
 * The user details service which builds the {@code UserDetails} instance from the supplied
 * authentication token.
 * 
 * If required, the authorization roles ought to be loaded from an external data-source as opposed to 
 * hard-coded here for demo purposes.
 * 
 * @author David.Tegart
 */
public class StudentCasAuthenticationUserDetailsService
		implements AuthenticationUserDetailsService<CasAssertionAuthenticationToken> {

	private static final Logger log = LoggerFactory.getLogger(StudentCasAuthenticationUserDetailsService.class);
	
	private List<String> allowedAttributes = new ArrayList<>();

	public StudentCasAuthenticationUserDetailsService() {}

	public StudentCasAuthenticationUserDetailsService(List<String> allowedAttributes) {
		this.allowedAttributes = Optional.ofNullable(allowedAttributes).orElse(new ArrayList<>());
	}

	@Override
	public UserDetails loadUserDetails(CasAssertionAuthenticationToken token) throws UsernameNotFoundException {

		Validate.notNull(token);
		Validate.notNull(token.getPrincipal());
		Validate.notNull(token.getAssertion());

		StudentUserDetails studentUserDetails = new StudentUserDetails(token.getPrincipal().toString(), "", loadAuthorities());

		Map<String, ?> attributes = token.getAssertion().getPrincipal().getAttributes();

		if ((allowedAttributes.size() > 0) && (attributes != null)) {

			Iterator<?> attributeNames = attributes.keySet().iterator();
			for (; attributeNames.hasNext();) {

				String attributeName = (String) attributeNames.next();

				addAttributeToUserDetails(attributeName, attributes.get(attributeName), studentUserDetails);
			}
		}

		return studentUserDetails;
	}

	private void addAttributeToUserDetails(String attributeName, Object attributeValue, StudentUserDetails studentUserDetails) {

		if (!allowedAttributes.contains(attributeName)) { // Ignore those attributes which aren't permitted
			
			log.debug("The Cas Attribute {} is not allowed.", attributeName);			
			
			return;
		}
		
		if(attributeName.equalsIgnoreCase(ATTRIBUTE_NAME) && (attributeValue instanceof String)) {
			
			log.debug("Student name attribute is |{}|", attributeValue);
			
			studentUserDetails.setName((String) attributeValue);
			
		} else if(attributeName.equalsIgnoreCase(ATTRIBUTE_STUDENT_ID) && (attributeValue instanceof String)) {
			
			log.debug("Student Id attribute is |{}|", attributeValue);
			
			studentUserDetails.setStudentId((String) attributeValue);
			
		} // Ignore all others for now
			
		return;
	}
	
	private List<SimpleGrantedAuthority> loadAuthorities() { // Add a simple role for demo. In reality, load from data-store.
		return Stream.of(new SimpleGrantedAuthority(ROLE_STUDENT)).collect(collectingAndThen(toList(), Collections::unmodifiableList));
	}
}