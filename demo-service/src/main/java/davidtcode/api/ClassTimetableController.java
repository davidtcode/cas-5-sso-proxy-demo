package davidtcode.api;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import static java.util.Optional.ofNullable;

/**
 * A demo controller which return timetable information for a given student.
 *  
 * @author David.Tegart
 */
@RestController
@RequestMapping("/api/timetable")
public class ClassTimetableController {

	private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

	private Map<Long, String> nextClassForUser = new ConcurrentHashMap<>();
	{
		nextClassForUser.put(101L, "Your next class is Mathematics and starts in 23 minutes ... ");
	}

	@RequestMapping(value = "/{userId}", method = RequestMethod.GET)
	public String getNextItemForUser(@PathVariable("userId") long userId) {
		return new StringBuilder(LocalDateTime.now().format(formatter))
				.append(": ")
				.append(ofNullable(nextClassForUser.get(userId)).orElse("There is no timetable for this user."))
				.toString();
	}
}