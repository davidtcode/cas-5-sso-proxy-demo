package davidtcode.service;

import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import davidtcode.config.ServiceProperties;

/**
 * A simple demo service gets the imaginary timetable information for a student.
 * 
 * @author David.Tegart
 */
@Service
public class TimetableItemService extends BaseService {

	private final RestTemplate restTemplate;
	
	@Autowired
	private ServiceProperties serviceProperties;

	public TimetableItemService(RestTemplateBuilder restTemplateBuilder) {
		this.restTemplate = restTemplateBuilder.build();
	}

	public String getNextItemOnTimetable(String studentId) {

		Validate.notBlank(studentId);
		
		String nextItemOnTimeTable = null;
		try {
			
			nextItemOnTimeTable = restTemplate.getForObject(
					buildUrlWithProxyTicket(serviceProperties.getTimetableUrlPrefix(), serviceProperties.getTimetableCasServiceUrl(), studentId), String.class);

		} catch (Exception ex) {
			
			String message = new StringBuilder("Next item on timetable for student Id").append(studentId).append(" cannot be obtained").toString();
			
			log.error(message, ex);
			
			throw new RuntimeException(message, ex);
		}

		return nextItemOnTimeTable;
	}
}