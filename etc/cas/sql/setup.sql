--
-- HSQLDB Setup script
--
-- Create the 'user' and 'user_attributes' tables and populate them
--

DROP TABLE IF EXISTS users;
CREATE TABLE users (
  id INT NOT NULL IDENTITY,
  username VARCHAR(50) NOT NULL,
  password VARCHAR(50) NOT NULL,
  UNIQUE (username)
);


DROP TABLE IF EXISTS user_attributes;
CREATE TABLE user_attributes (
  id INT NOT NULL IDENTITY,
  username VARCHAR(50) NOT NULL,
  attrname VARCHAR(50) NOT NULL,
  attrvalue VARCHAR(50) NOT NULL
);

INSERT INTO users (id, username, password) VALUES (1, 'demo@demo.com', 'password');
INSERT INTO users (id, username, password) VALUES (2, 'admin@admin.com', 'password');

INSERT INTO user_attributes (id, username, attrname, attrvalue) VALUES (1, 'demo@demo.com', 'name', 'Jack Brown');
INSERT INTO user_attributes (id, username, attrname, attrvalue) VALUES (2, 'demo@demo.com', 'studentId', '101');
INSERT INTO user_attributes (id, username, attrname, attrvalue) VALUES (3, 'demo@demo.com', 'phone', '+353 01 111111');