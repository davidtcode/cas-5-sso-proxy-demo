package davidtcode.config;

import javax.validation.constraints.NotNull;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

/**
 * Contains the properties required by the portal to talk with associated services.
 * 
 * @author David.Tegart
 */
@Configuration
@ConfigurationProperties(prefix = "demo.service", ignoreUnknownFields = false)
@Validated
public class ServiceProperties {

	@NotNull
	private String emailUrlPrefix;
	
	@NotNull
	private String emailCasServiceUrl;
	
	@NotNull
	private String timetableUrlPrefix;
		
	@NotNull
	private String timetableCasServiceUrl;

	public String getEmailUrlPrefix() {
		return emailUrlPrefix;
	}

	public void setEmailUrlPrefix(String emailUrlPrefix) {
		this.emailUrlPrefix = emailUrlPrefix;
	}

	public String getEmailCasServiceUrl() {
		return emailCasServiceUrl;
	}

	public void setEmailCasServiceUrl(String emailCasServiceUrl) {
		this.emailCasServiceUrl = emailCasServiceUrl;
	}

	public String getTimetableUrlPrefix() {
		return timetableUrlPrefix;
	}

	public void setTimetableUrlPrefix(String timetableUrlPrefix) {
		this.timetableUrlPrefix = timetableUrlPrefix;
	}

	public String getTimetableCasServiceUrl() {
		return timetableCasServiceUrl;
	}

	public void setTimetableCasServiceUrl(String timetableCasServiceUrl) {
		this.timetableCasServiceUrl = timetableCasServiceUrl;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ServiceProperties [emailUrlPrefix=");
		builder.append(emailUrlPrefix);
		builder.append(", emailCasServiceUrl=");
		builder.append(emailCasServiceUrl);
		builder.append(", timetableUrlPrefix=");
		builder.append(timetableUrlPrefix);
		builder.append(", timetableCasServiceUrl=");
		builder.append(timetableCasServiceUrl);
		builder.append("]");
		return builder.toString();
	}
}