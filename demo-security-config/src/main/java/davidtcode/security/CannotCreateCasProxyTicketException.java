package davidtcode.security;

/**
 * @author David.Tegart
 */
public class CannotCreateCasProxyTicketException extends Exception {

	private static final long serialVersionUID = 1L;

	public CannotCreateCasProxyTicketException(String message) {
		super(message);
	}

	public CannotCreateCasProxyTicketException(Throwable cause) {
		super(cause);
	}

	public CannotCreateCasProxyTicketException(String message, Throwable cause) {
		super(message, cause);
	}
}