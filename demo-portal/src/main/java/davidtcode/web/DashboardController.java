package davidtcode.web;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import davidtcode.security.CasAuthenticationFacade;
import davidtcode.service.EmailStatusService;
import davidtcode.service.TimetableItemService;

/**
 * The controller which supplies the demo portal or dashboard.
 * 
 * @author David.Tegart
 */
@Controller
public class DashboardController {

	@Autowired
	private EmailStatusService emailStatusService;

	@Autowired
	private TimetableItemService timetableItemService;

	@Autowired
	private CasAuthenticationFacade authenticationFacade;

	@RequestMapping("/")
	public String dashboard(HttpServletRequest request, Model model) {

		String studentId = authenticationFacade.getStudentId().orElse("");
		
		model.addAttribute("studentName", authenticationFacade.getStudentName().orElse(""));
		model.addAttribute("studentId", studentId);
		
		model.addAttribute("emailStatus", emailStatusService.getStatusForStudentId(studentId));
		model.addAttribute("classTimetable", timetableItemService.getNextItemOnTimetable(studentId));

		return "dashboard";
	}
}