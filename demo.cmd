@echo off

REM ---------------------------------------------------------------------
REM	Define variables 
REM ---------------------------------------------------------------------

@set CAS_SERVER_PORT=3000
@set PORTAL_PORT=3001
@set EMAIL_SERVICE_PORT=3002
@set TIMETABLE_PORT=3003

REM ---------------------------------------------------------------------
REM	Commands 
REM ---------------------------------------------------------------------

@if "%1" == "package" call:package
@if "%1" == "run" call:run %2 %3 %4

@rem function section starts here
@goto:eof

:package
    
	call mvnw.cmd -f ./demo-cas-overlay/pom.xml clean package 
	call mvnw.cmd -f ./demo-security-config/pom.xml clean package install
	call mvnw.cmd -f ./demo-service/pom.xml clean package
	call mvnw.cmd -f ./demo-portal/pom.xml clean package
	
	exit /B %ERRORLEVEL%
@goto:eof

:run

	start "CAS Server" java -Xms500m -Xmx1g -jar ./demo-cas-overlay/target/demo-cas-overlay-1.0.war --cas.standalone.config=etc/cas/config -Dserver.port=%CAS_SERVER_PORT% --server.port=%CAS_SERVER_PORT%	
	start "Email Service" java -jar ./demo-service/target/demo-service-1.0.jar --server.port=%EMAIL_SERVICE_PORT%
	start "Timetable Service" java -jar ./demo-service/target/demo-service-1.0.jar --server.port=%TIMETABLE_PORT%
	start "Portal" java -jar ./demo-portal/target/demo-portal-1.0.jar --server.port=%PORTAL_PORT% --cas.server.port=%CAS_SERVER_PORT% --email.server.port=%EMAIL_SERVICE_PORT% --timetable.server.port=%TIMETABLE_PORT%

	exit /B %ERRORLEVEL%
@goto:eof