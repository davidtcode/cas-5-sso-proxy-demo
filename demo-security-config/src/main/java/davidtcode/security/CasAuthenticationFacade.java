package davidtcode.security;

import java.util.Optional;

/**
 * Defines a single point from which needed cas-related authentication information can be obtained.
 * 
 * @author David.Tegart
 */
public interface CasAuthenticationFacade {

	public boolean isAuthenticated();
	
	public Optional<StudentUserDetails> getStudentUserDetails();
	
	public Optional<String> getStudentId();
	
	public Optional<String> getStudentName();
	
	public String getProxyTicket(String serviceUrlToGetProxyTicketFor) throws CannotCreateCasProxyTicketException;
}