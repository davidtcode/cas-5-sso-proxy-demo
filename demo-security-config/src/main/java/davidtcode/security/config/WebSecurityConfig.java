package davidtcode.security.config;

import org.jasig.cas.client.session.SingleSignOutFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.logout.LogoutFilter;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;

import davidtcode.security.config.SecurityProperties;

/**
 * Creates the required configuration to secure this web application.
 *   
 * @author David.Tegart
 */
@Configuration
@EnableWebSecurity
@EnableConfigurationProperties(SecurityProperties.class)
@Import(CasAuthenticationConfig.class)
class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private SecurityProperties securityProperties;
	
	@Autowired
	private CasAuthenticationConfig casAuthenticationConfig;

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http.exceptionHandling()
			.authenticationEntryPoint(casAuthenticationConfig.casAuthenticationEntryPoint())
			.and()
				.authorizeRequests()
					.anyRequest().authenticated()
				.and()
					.addFilterBefore(requestCasSingleLogoutFilter(), LogoutFilter.class)
					.addFilterAfter(singleSignOutFilter(), LogoutFilter.class)
					.addFilterAfter(casAuthenticationConfig.casAuthenticationFilter(), SingleSignOutFilter.class);

		http.headers().frameOptions().disable(); // Enable on production

		http
			.logout()
			.logoutUrl(securityProperties.getLogoutPath())
				.logoutSuccessUrl(securityProperties.getLogoutSuccessUrl())
				.invalidateHttpSession(true)
				.deleteCookies("JSESSIONID");
	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/webjars/**");
	}

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(casAuthenticationConfig.casAuthenticationProvider());
	}

	@Bean
	public SingleSignOutFilter singleSignOutFilter() { // Handles a Single Logout Request from the CAS Server

		SingleSignOutFilter singleSignOutFilter = new SingleSignOutFilter();
		singleSignOutFilter.setCasServerUrlPrefix(securityProperties.getCasUrlPrefix());
		singleSignOutFilter.setIgnoreInitConfiguration(true);

		return singleSignOutFilter;
	}

	@Bean
	public LogoutFilter requestCasSingleLogoutFilter() { // Redirects to the CAS Server to signal Single Logout should be performed

		LogoutFilter logoutFilter = new LogoutFilter(securityProperties.getCasLogoutUrl(), new SecurityContextLogoutHandler());
		logoutFilter.setFilterProcessesUrl(securityProperties.getLogoutPath());

		return logoutFilter;
	}
}