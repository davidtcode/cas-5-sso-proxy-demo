package davidtcode.security.config;

import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import org.apache.commons.lang3.Validate;
import org.jasig.cas.client.proxy.ProxyGrantingTicketStorage;
import org.jasig.cas.client.proxy.ProxyGrantingTicketStorageImpl;
import org.jasig.cas.client.validation.Cas30ProxyTicketValidator;
import org.jasig.cas.client.validation.Cas30ServiceTicketValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.cas.ServiceProperties;
import org.springframework.security.cas.authentication.CasAssertionAuthenticationToken;
import org.springframework.security.cas.authentication.CasAuthenticationProvider;
import org.springframework.security.cas.authentication.CasAuthenticationToken;
import org.springframework.security.cas.authentication.EhCacheBasedTicketCache;
import org.springframework.security.cas.authentication.StatelessTicketCache;
import org.springframework.security.cas.web.CasAuthenticationEntryPoint;
import org.springframework.security.cas.web.CasAuthenticationFilter;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;

import davidtcode.security.CannotCreateCasProxyTicketException;
import davidtcode.security.CasAuthenticationFacade;
import davidtcode.security.StudentCasAuthenticationUserDetailsService;
import davidtcode.security.StudentUserDetails;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.config.CacheConfiguration;
import net.sf.ehcache.util.concurrent.ConcurrentHashMap;

/**
 * Defines the essential configuration required to enable Cas Single Sign On as well as Proxy Authentication.
 *   
 * @author David.Tegart
 */
@Configuration
@Order(101)
public class CasAuthenticationConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private SecurityProperties securityProperties;

	@Bean
	public CasAuthenticationProvider casAuthenticationProvider() {

		CasAuthenticationProvider casAuthenticationProvider = new CasAuthenticationProvider();
		casAuthenticationProvider.setAuthenticationUserDetailsService(authenticationUserDetailsService());
		casAuthenticationProvider.setServiceProperties(serviceProperties());
		casAuthenticationProvider.setTicketValidator(cas30ProxyTicketValidator());
		casAuthenticationProvider.setKey(UUID.randomUUID().toString());
		casAuthenticationProvider.setStatelessTicketCache(statelessTicketCache());

		return casAuthenticationProvider;
	}

	@Bean
	public ServiceProperties serviceProperties() {

		ServiceProperties serviceProperties = new ServiceProperties();

		serviceProperties.setService(securityProperties.getServiceUrlForCas()); // A redirect-loop will occur if this is incorrect
		serviceProperties.setSendRenew(false);
		serviceProperties.setAuthenticateAllArtifacts(true);

		return serviceProperties;
	}

	@Bean
	public CasAuthenticationFilter casAuthenticationFilter() throws Exception {

		CasAuthenticationFilter casAuthenticationFilter = new CasAuthenticationFilter();

		casAuthenticationFilter.setAuthenticationManager(authenticationManager());
		casAuthenticationFilter.setProxyGrantingTicketStorage(proxyGrantingTicketStorage());
		casAuthenticationFilter.setProxyReceptorUrl(securityProperties.getProxyReceptorPath());
		casAuthenticationFilter.setServiceProperties(serviceProperties());

		return casAuthenticationFilter;
	}

	@Bean
	public CasAuthenticationEntryPoint casAuthenticationEntryPoint() {

		CasAuthenticationEntryPoint casAuthenticationEntryPoint = new CasAuthenticationEntryPoint();
		casAuthenticationEntryPoint.setLoginUrl(securityProperties.getCasLoginUrl());
		casAuthenticationEntryPoint.setServiceProperties(serviceProperties());

		return casAuthenticationEntryPoint;
	}
	
	@Bean
	public AuthenticationUserDetailsService<CasAssertionAuthenticationToken> authenticationUserDetailsService() {
		return new StudentCasAuthenticationUserDetailsService(securityProperties.getCasAttributes().getAllowedAttributes());
	}

	@Bean
	public ProxyGrantingTicketStorage proxyGrantingTicketStorage() {
		return new ProxyGrantingTicketStorageImpl();
	}

	@Bean
	public Cas30ProxyTicketValidator cas30ProxyTicketValidator() {

		Cas30ProxyTicketValidator cas30ProxyTicketValidator = new Cas30ProxyTicketValidator(securityProperties.getCasUrlPrefix());

		cas30ProxyTicketValidator.setProxyGrantingTicketStorage(proxyGrantingTicketStorage());
		cas30ProxyTicketValidator.setProxyCallbackUrl(securityProperties.getProxyCallbackUrl());
		cas30ProxyTicketValidator.setAcceptAnyProxy(true); // Not suitable for non-demo purposes

		return cas30ProxyTicketValidator;
	}

	@Bean
	public Cas30ServiceTicketValidator cas30ServiceTicketValidator() {
		return new Cas30ServiceTicketValidator(securityProperties.getCasUrlPrefix());
	}

	@Bean
	public StatelessTicketCache statelessTicketCache() {

		EhCacheBasedTicketCache statelessTicketCache = new EhCacheBasedTicketCache();
		statelessTicketCache.setCache(ticketCache());

		return statelessTicketCache;
	}

	@Bean(destroyMethod = "shutdown")
	public CacheManager ehCacheManager() {

		CacheConfiguration cacheConfiguration = new CacheConfiguration();
		cacheConfiguration.setName("casTickets");
		cacheConfiguration.setMemoryStoreEvictionPolicy("LRU");
		cacheConfiguration.setMaxEntriesLocalHeap(500);

		net.sf.ehcache.config.Configuration configuration = new net.sf.ehcache.config.Configuration();
		configuration.addCache(cacheConfiguration);

		return CacheManager.newInstance(configuration);
	}

	@Bean(initMethod = "initialise", destroyMethod = "dispose")
	public Cache ticketCache() {

		Cache cache = new Cache("casTickets", 50, true, false, 3600, 900);
		cache.setCacheManager(ehCacheManager());

		return cache;
	}

	@Bean
	public CasAuthenticationFacade authenticationFacade() {

		return new CasAuthenticationFacade() {

			private final Logger log = LoggerFactory.getLogger(CasAuthenticationFacade.class);

			private Map<String, String> cachedProxyTickets = new ConcurrentHashMap<>(); // Simple map for demo.

			@Override
			public boolean isAuthenticated() {

				SecurityContext securityContext = SecurityContextHolder.getContext();

				if (securityContext == null) {
					return false;
				}

				if ((securityContext.getAuthentication() != null)
						&& (securityContext.getAuthentication().isAuthenticated()
								&& !(securityContext.getAuthentication() instanceof AnonymousAuthenticationToken))) {
					return true;
				}

				return false;
			}

			@Override
			public Optional<StudentUserDetails> getStudentUserDetails() {

				if (!isAuthenticated()) {
					return Optional.empty();
				}

				Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

				if (principal instanceof StudentUserDetails) {
					return Optional.of((StudentUserDetails) principal);
				}

				log.warn("The principal {} is not of the required StudentUserDetails type", principal);

				return Optional.empty();
			}

			@Override
			public Optional<String> getStudentId() {

				Optional<StudentUserDetails> studentUserDetails = getStudentUserDetails();

				if (studentUserDetails.isPresent()) {
					return Optional.ofNullable(studentUserDetails.get().getStudentId());
				}

				return Optional.empty();
			}

			@Override
			public Optional<String> getStudentName() {

				Optional<StudentUserDetails> studentUserDetails = getStudentUserDetails();

				if (studentUserDetails.isPresent()) {
					return Optional.ofNullable(studentUserDetails.get().getName());

				}

				return Optional.empty();
			}

			@Override
			public String getProxyTicket(String serviceUrlToGetProxyTicketFor)
					throws CannotCreateCasProxyTicketException {

				Validate.notBlank(serviceUrlToGetProxyTicketFor);

				//
				// Proxy tickets can be re-used. Calling getProxyTicketFor below results in a
				// call to the cas server,
				// which should be avoided. So proxy tickets are cached, keyed on the url of the
				// service the portal is
				// attempting to contact.

				String proxyTicket;
				if ((proxyTicket = cachedProxyTickets.get(serviceUrlToGetProxyTicketFor)) != null) {

					log.debug("Found cached proxy ticket |{}| for |{}|", proxyTicket, serviceUrlToGetProxyTicketFor);

					return proxyTicket;
				}

				//
				// We have no cached proxy ticket, so create one and then cache it

				try {

					Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

					CasAuthenticationToken casAuthenticationToken = (CasAuthenticationToken) authentication;

					proxyTicket = casAuthenticationToken.getAssertion().getPrincipal()
							.getProxyTicketFor(serviceUrlToGetProxyTicketFor);

					log.debug("Caching proxy ticket |{}| for |{}|", proxyTicket, serviceUrlToGetProxyTicketFor);

					cachedProxyTickets.put(serviceUrlToGetProxyTicketFor, proxyTicket);

				} catch (Exception ex) {

					log.error("A proxy ticket cannot be created for serviceUrlToGetProxyTicketFor |{}|",
							serviceUrlToGetProxyTicketFor);

					throw new CannotCreateCasProxyTicketException(ex);
				}

				return proxyTicket;
			}
		};
	}
}