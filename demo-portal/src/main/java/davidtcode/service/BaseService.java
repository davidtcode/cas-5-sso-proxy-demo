package davidtcode.service;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import davidtcode.security.CannotCreateCasProxyTicketException;
import davidtcode.security.CasAuthenticationFacade;

/**
 * @author David.Tegart
 */
public abstract class BaseService {

	protected static final Logger log = LoggerFactory.getLogger(BaseService.class);
	
	@Autowired
	protected CasAuthenticationFacade casAuthenticationFacade;
	
	protected String buildUrlWithProxyTicket(String urlPrefix, String destinationServceUrl, String studentId) 
			throws UnsupportedEncodingException, CannotCreateCasProxyTicketException {

		StringBuilder url = new StringBuilder();

		url.append(urlPrefix);
		url.append(studentId);
		url.append("?ticket=");

		String proxyTicket = casAuthenticationFacade.getProxyTicket(destinationServceUrl);

		url.append(URLEncoder.encode(proxyTicket, "UTF-8"));

		log.debug("The generated url is |{}|", url.toString());

		return url.toString();
	}
}