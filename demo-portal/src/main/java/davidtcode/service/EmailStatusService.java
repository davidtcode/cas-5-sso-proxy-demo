package davidtcode.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import davidtcode.config.ServiceProperties;

/**
 * A simple demo service gets the imaginary email status for a student.
 * 
 * @author David.Tegart
 */
@Service
public class EmailStatusService extends BaseService {

	private final RestTemplate restTemplate;

	@Autowired
	private ServiceProperties serviceProperties;

	public EmailStatusService(RestTemplateBuilder restTemplateBuilder) {
		this.restTemplate = restTemplateBuilder.build();
	}

	public String getStatusForStudentId(String studentId) {

		String emailStatus = null;
		try {
			
			emailStatus = restTemplate.getForObject(
					buildUrlWithProxyTicket(serviceProperties.getEmailUrlPrefix(), serviceProperties.getEmailCasServiceUrl(), studentId), String.class);

		} catch (Exception ex) {
			
			String message = new StringBuilder("Email status for student Id ").append(studentId).append(" cannot be obtained").toString();
			
			log.error(message, ex);
			
			throw new RuntimeException(message, ex);
		}

		return emailStatus;
	}
}