package davidtcode.api;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import static java.util.Optional.ofNullable;

/**
 * A demo controller which return email status information for a given student.
 *  
 * @author David.Tegart
 */
@RestController
@RequestMapping("/api/email")
public class EmailStatusController {
	
	private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

	private Map<Long, String> emailStatuses = new ConcurrentHashMap<>();
	{
		emailStatuses.put(101L, "You have 10 unread emails.");
	}

	@RequestMapping(value = "/status/{userId}", method = RequestMethod.GET)
	public String getEmailStatusForUser(@PathVariable("userId") long userId) {
		return new StringBuilder(LocalDateTime .now().format(formatter))
				.append(": ")
				.append(ofNullable(emailStatuses.get(userId)).orElse("There is no email status for this user."))
				.toString();
	}
}