package davidtcode.security;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

/**
 * @author David.Tegart
 */
public class StudentUserDetails extends User {

	public static final String ROLE_STUDENT = "ROLE_STUDENT";
	
	public static final String ATTRIBUTE_NAME = "name";
	public static final String ATTRIBUTE_STUDENT_ID = "studentId";
	
	private String studentId;
	private String name;
	
	public StudentUserDetails(String username, String password, Collection<? extends GrantedAuthority> authorities) {		
		super(username, password, authorities);
	}

	public StudentUserDetails(String username, String password, Collection<? extends GrantedAuthority> authorities,
			String studentId, String name) {
		
		super(username, password, authorities);
		
		this.studentId = studentId;
		this.name = name;
	}

	public StudentUserDetails(String username, String password, boolean enabled, boolean accountNonExpired,
			boolean credentialsNonExpired, boolean accountNonLocked,
			Collection<? extends GrantedAuthority> authorities) {
		super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
	}

	public String getStudentId() {
		return studentId;
	}

	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}